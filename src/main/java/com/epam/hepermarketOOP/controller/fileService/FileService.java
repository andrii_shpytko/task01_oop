package com.epam.hepermarketOOP.controller.fileService;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileService {
    private static final String FILE_NAME = ".\\src\\main\\java\\com\\epam\\com.epam.hepermarketOOP\\fileHistory\\" + generateFileName()+".txt";

    private static String generateFileName(){
        return DateTimeFormatter.ofPattern("yyyyMMddHHmm").format(LocalDateTime.now());
    }

    public void getFileName(){
        DateTimeFormatter timeStampPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String format = timeStampPattern.format(LocalDateTime.now());
        System.out.println(format);
    }

    public void writer(String text){
        try(FileWriter fileWriter = new FileWriter(FILE_NAME)) {
            fileWriter.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
