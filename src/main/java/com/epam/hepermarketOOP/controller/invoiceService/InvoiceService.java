package com.epam.hepermarketOOP.controller.invoiceService;

import com.epam.hepermarketOOP.movel.hypermarket.Hypermarket;
import com.epam.hepermarketOOP.movel.product.Product;
import com.epam.hepermarketOOP.movel.product.productProperties.ProductType;
import com.epam.hepermarketOOP.movel.services.HypermarketService;
import com.epam.hepermarketOOP.movel.services.PriceHypermarketService;
import com.epam.hepermarketOOP.controller.fileService.FileService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class InvoiceService {
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static PriceHypermarketService priceHypermarketService = new PriceHypermarketService();
    private static HypermarketService hypermarketService = new HypermarketService();
    private FileService fileService = new FileService();

    private static final String SELECT_NUMBER_OPERATION = "\tPlease an action by typing [0 - 6]:\t";

    public void runApplication() {
        int menuSize = MenuService.values().length;
        boolean continueLoop = true;
        Hypermarket hypermarket = new Hypermarket();
        List<Product> products = new LinkedList<>();
        showMenu();
        int param = inputNumber(br);
        while (param != 0) {
            switch (param) {
                case 1:
                    System.out.println("Add product to your list:");
                    products = addInInvoice();
                    hypermarketService.addAircraft(hypermarket, products);
                    System.out.print(SELECT_NUMBER_OPERATION);
                    showMenu();
                    param = inputNumber(br);
                    break;
                case 2:
                    System.out.println("Print elements!");
                    hypermarketService.showAirline(hypermarket);
                    System.out.print(SELECT_NUMBER_OPERATION);
                    showMenu();
                    param = inputNumber(br);
                    break;
                case 3:
                    System.out.println("Delete element number ");
                    try {
                        hypermarketService.removeElement(hypermarket, inputNumber(br));
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                    System.out.print(SELECT_NUMBER_OPERATION);
                    param = inputNumber(br);
                    break;
                case 4:
                    System.out.println("Select the type of sorting");
                    hypermarketService.showSortedType();
                    hypermarketService.sorted(hypermarket, inputNumber(br));
                    System.out.println(SELECT_NUMBER_OPERATION);
                    showMenu();
                    param = inputNumber(br);
                    break;
                case 5:
                    System.out.println("You price: ");
                    priceHypermarketService.generatePrice(hypermarket);
                    Long price = hypermarket.getPrice();
                    System.out.printf("You price : %,d\n", price);
                    System.out.println(SELECT_NUMBER_OPERATION);
                    showMenu();
                    param = inputNumber(br);
                    break;
                case 6:
                    System.out.println("Write your invoice in check-file!");
                    fileService.writer(hypermarket.getProducts().toString());
                    System.out.print(SELECT_NUMBER_OPERATION);
                    showMenu();
                    param = inputNumber(br);
                    break;
                default:
                    param = 0;
                    System.out.println("Thank you for using our store!");
                    break;
            }
        }
    }

    public List<Product> addInInvoice() {
        List<Product> myProductList = new ArrayList<>();
        int menuSize = ProductType.values().length;
        showCatalog();
        int param = inputNumber(br);
        switch (param) {
            case 0:
                System.out.println("You add to invoice: " + ProductType.BATH_RAVAK_CAMPANULA.toString());
                myProductList.add(new Product(ProductType.BATH_RAVAK_CAMPANULA));
                break;
            case 1:
                System.out.println("You add to invoice: " + ProductType.BOWL_JACOB_DELAFON_PANACHE_E1434.toString());
                myProductList.add(new Product(ProductType.BOWL_JACOB_DELAFON_PANACHE_E1434));
                break;
            case 2:
                System.out.println("You add to invoice: " + ProductType.WASHBASIN_Roca_Gap.toString());
                myProductList.add(new Product(ProductType.WASHBASIN_Roca_Gap));
                break;
            case 3:
                System.out.println("You add to invoice: " + ProductType.DOOR_TERMINUS90.toString());
                myProductList.add(new Product(ProductType.DOOR_TERMINUS90));
                break;
            case 4:
                System.out.println("You add to invoice: " + ProductType.DOOR_TERMINUS80.toString());
                myProductList.add(new Product(ProductType.DOOR_TERMINUS80));
                break;
            default:
                System.out.println("You has entered incorrect id!");
                break;
        }
        return myProductList;
    }

    public void showCatalog() {
        ProductType[] values = ProductType.values();
        for (int i = 0; i < values.length; i++) {
            System.out.print("#" + i + ". " + values[i] + "\t");
        }
        System.out.println();
    }

    public void showMenu() {
        MenuService[] values = MenuService.values();
        for (int i = 0; i < values.length; i++) {
            System.out.print("#" + i + ". " + values[i] + "\t");
        }
        System.out.println();
    }

    private static int inputNumber(BufferedReader br) {
        int number = 0;
        boolean continueLoop = true;
        do {
            System.out.print("\tInput number: ");
            number = validateNumber(br);
            continueLoop = false;
            return number;
        } while (continueLoop);
    }

    private static int validateNumber(BufferedReader br) {
        int number = 0;
        boolean continueLoop = true;
        do {
            try {
                number = Integer.parseInt(br.readLine());
                continueLoop = false;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                System.out.println("Exception " + e);
                e.printStackTrace();
                System.out.print("You input incorrect value! Please input Integer value: ");
            }
        } while (continueLoop);
        return number;
    }


}
