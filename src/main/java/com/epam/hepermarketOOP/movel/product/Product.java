package com.epam.hepermarketOOP.movel.product;

import com.epam.hepermarketOOP.movel.product.productProperties.ProductType;

public class Product {
    private ProductType type;
    private int price;

    public Product() {
    }

    public Product(ProductType type) {
        this.type = type;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return String.format("%s: price = %,d($)\n",
                getType(), getPrice());
    }


}
