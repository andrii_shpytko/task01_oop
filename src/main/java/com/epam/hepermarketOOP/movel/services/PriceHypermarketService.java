package com.epam.hepermarketOOP.movel.services;

import com.epam.hepermarketOOP.movel.product.Product;
import com.epam.hepermarketOOP.movel.hypermarket.Hypermarket;
import com.epam.hepermarketOOP.movel.services.productService.ProductGenerator;

public class PriceHypermarketService {
    private ProductGenerator productGenerator = new ProductGenerator();

    public void generatePrice(Hypermarket hypermarket) {
        Long price = 0L;
        for (Product product : hypermarket.getProducts()) {
            price += productGenerator.getPrice(product.getType());
        }
        hypermarket.setPrice(price);
    }
}
