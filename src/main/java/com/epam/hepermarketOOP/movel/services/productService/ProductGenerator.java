package com.epam.hepermarketOOP.movel.services.productService;

import com.epam.hepermarketOOP.movel.product.productProperties.ProductType;

import java.util.HashMap;
import java.util.Map;

public class ProductGenerator {
    private static Map<ProductType, Integer> price = new HashMap<>();


    static {
        price.put(ProductType.DOOR_TERMINUS80, 3_449);
        price.put(ProductType.DOOR_TERMINUS90, 3_408);
        price.put(ProductType.WASHBASIN_Roca_Gap, 2_200);
        price.put(ProductType.BATH_RAVAK_CAMPANULA, 11_242);
        price.put(ProductType.BOWL_JACOB_DELAFON_PANACHE_E1434, 12_690);
    }

    public Integer getPrice(ProductType type){
        return price.get(type);
    }
}
