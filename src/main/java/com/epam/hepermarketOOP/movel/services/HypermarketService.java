package com.epam.hepermarketOOP.movel.services;

import com.epam.hepermarketOOP.movel.hypermarket.Hypermarket;
import com.epam.hepermarketOOP.movel.product.Product;
import com.epam.hepermarketOOP.movel.services.productService.ProductGenerator;
import com.epam.hepermarketOOP.controller.invoiceService.SortedType;

import java.util.*;

public class HypermarketService {
    private ProductGenerator productGenerator = new ProductGenerator();

    private static final Comparator<Product> BY_PRICE =
            (Product a1, Product a2) -> a1.getPrice() - a2.getPrice();


    public void addAircraft(Hypermarket hypermarket, List<Product> products) {
        List<Product> productList = new ArrayList<>(products);
        productList
                .stream()
                .forEach((Product product) -> product.setPrice(productGenerator.getPrice(product.getType())));
        hypermarket.getProducts().addAll(products);
    }


    public void removeElement(Hypermarket hypermarket, int position) {
        List<Product> products = hypermarket.getProducts();
        if (products.size() >= position) {
            products.remove(position);
        } else {
            throw new IndexOutOfBoundsException("please enter correct number: [ >= 0 and <= " + hypermarket.getProducts().size() + " ]");
        }
    }

    public void showSortedType() {
        SortedType[] values = SortedType.values();
        for (int i = 0; i < values.length; i++) {
            int counter = i + 1;
            System.out.print("#" + counter + ". " + values[i] + "\t");
        }
        System.out.println();
    }

    public void sorted(Hypermarket hypermarket, int position) {
        List<Product> sortedProductList = hypermarket.getProducts();
        switch (position) {
            case 1:
                Collections.sort(sortedProductList, BY_PRICE);
                break;
            default:
                System.out.println("Sorry! Failed to sort");
        }
        showAircrafts(sortedProductList);
    }

    public void showAirline(Hypermarket hypermarket) {
        int counter = 0;
        for (Product product : hypermarket.getProducts()) {
            System.out.printf("#%d\t%s", counter, product);
            counter++;
        }
    }

    public void showAircrafts(List<Product> products) {
        int counter = 0;
        for (Product product : products) {
            System.out.printf("#%d\t%s", counter, product);
            counter++;
        }
    }


}
