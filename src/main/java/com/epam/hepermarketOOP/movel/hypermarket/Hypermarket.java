package com.epam.hepermarketOOP.movel.hypermarket;

import com.epam.hepermarketOOP.movel.product.Product;

import java.util.ArrayList;
import java.util.List;

public class Hypermarket {
    private List<Product> products = new ArrayList<>();
    private Long price;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
