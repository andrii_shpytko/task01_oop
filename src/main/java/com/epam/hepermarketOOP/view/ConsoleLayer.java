package com.epam.hepermarketOOP.view;

import com.epam.hepermarketOOP.controller.invoiceService.InvoiceService;

public class ConsoleLayer implements Layer {
    InvoiceService invoiceService = new InvoiceService();

    @Override
    public void showApplication(InvoiceService invoiceService) {
        invoiceService.runApplication();
    }

    public void run(){
        showApplication(invoiceService);
    }
}
